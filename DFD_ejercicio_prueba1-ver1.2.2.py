import os
acceso_denegado=True
convenio=False
despachos = {"ID":[],"Rut":[],"Nombre":[],"Eleccion_huevo":[],"convenio":[],
             "direccion":[],"fecha_despacho":[],"cantidad_huevos":[],"total_compra":[]}
huevos=[["Gallina","Pato","Codorniz","Avestruz"],[50,50,150,800]]
i=1
intentos=4
identificador=0
while (i<intentos):
    usuario_predeterminado=input("Usuario: ")
    contraseña_predeterminada=input("Contraseña: ")
    if contraseña_predeterminada == "huevos1994" and usuario_predeterminado=="admin":
        print("Acceso autorizado")
        acceso_denegado=False
        i=intentos
    else:
        print("Acceso denegado")
        if i==3:
            print("Maximo de intentos alcanzados")
        i+=1
    
if  acceso_denegado==False:
    opcion='';
    while opcion != "e":
        os.system ("cls")
        print("---------------------------------------------------")
        print("---All Eggs - Sistema de gestion---")
        print("---------------------------------------------------")
        print("Menú")
        print("a) Asignacion de precios de huevos")
        print("b) Creacion de despachos")
        print("c) Listar huevos")
        print("d) Despachos")
        print("e) Salir")
        print("---------------------------------------------------")
        opcion=input("Ingrese una opcion: ")
        
        if opcion=='e':
            os.system ("cls")
            print("Sistema cerrado")

        if opcion=="a":
            opcion_huevos=""
            while opcion_huevos != "5":
                os.system ("cls")
                print("---------------------------------------------------")
                print("1) Huevos de Gallina -----$50")
                print("2) Huevos de Pato    -----$50")
                print("3) Huevos de Codorniz-----$150")
                print("4) Huevos de Avestruz-----$800")
                print("5) Terminar")
                print("---------------------------------------------------")
                opcion_huevos=input("Ingrese el tipo de huevo: ")
                print("---------------------------------------------------")
                nuevo_valor=0
                if opcion_huevos=="1":
                    print("---Huevos de Gallina---")
                    while nuevo_valor<huevos[1][0]:
                        nuevo_valor=int(input("Ingrese nuevo valor: "))
                        if nuevo_valor>=50:
                            huevos[1][0]=nuevo_valor
                        else:
                            print("Valor no puede ser inferior a $50")
                    
                if opcion_huevos=="2":
                    print("---Huevos de Pato---")
                    while nuevo_valor<huevos[1][1]:
                        nuevo_valor=int(input("Ingrese nuevo valor: "))
                        if nuevo_valor>=50:
                            huevos[1][1]=nuevo_valor
                        else:
                            print("Valor no puede ser inferior a $50")
                if opcion_huevos=="3":
                    print("---Huevos de Codorniz---")
                    while nuevo_valor<huevos[1][2]:
                        nuevo_valor=int(input("Ingrese nuevo valor: "))
                        if nuevo_valor>=150:
                            huevos[1][2]=nuevo_valor
                        else:
                            print("Valor no puede ser inferior a $150")
                if opcion_huevos=="4":
                    print("---Huevos de Avestruz---")
                    while nuevo_valor<huevos[1][3]:
                        nuevo_valor=int(input("Ingrese nuevo valor: "))
                        if nuevo_valor>=800:
                            huevos[1][3]=nuevo_valor
                        else:
                            print("Valor no puede ser inferior a $800")
                
                
        if opcion=="b":
            os.system ("cls")
            cantidad_correcta=False
            cantidad_huevos=0
            print("---Creacion de despachos---")
            print("")
            identificador=len(despachos["ID"])
            print("ID: ",identificador)
            rut_cliente=input("Ingrese Rut sin digito verificador: ")
            nombre_cliente=input("Ingrese nombre o razon social: ")
            huevo_eleccion=input("Ingrese un tipo de huevo: ")
            convenio_activo=input("¿Tiene Convenio?(S/N): ")
            direccion_cliente=input("Ingrese direccion de despacho: ")
            fecha_despacho=input("Ingrese fecha de despacho(dd/mm/aaaa): ")
            while cantidad_correcta!=True:
                cantidad_huevos=int(input("Ingrese cantidad de huevos: "))
                if cantidad_huevos >= 50 and cantidad_huevos <= 10000:
                    cantidad_correcta=True
                    despachos["cantidad_huevos"].append(cantidad_huevos)
                else:
                    print("Cantidad incorrecta")
                    
            despachos["ID"].append(identificador)
            despachos["Rut"].append(rut_cliente)
            despachos["Nombre"].append(nombre_cliente)
            despachos["Eleccion_huevo"].append(huevo_eleccion)
            despachos["direccion"].append(direccion_cliente)
            despachos["fecha_despacho"].append(fecha_despacho)
            
            if convenio_activo=="S" or convenio_activo=="s":
                despachos["convenio"].append("Si")
                convenio=True
            else:
                despachos["convenio"].append("No")

            tamaño = 4
            for i in range(tamaño):
                huevo_eleccion=huevo_eleccion.lower()
                huevo_comp=huevos[0][i].lower()
                if huevo_eleccion==huevo_comp:
                    total_compra=(cantidad_huevos*huevos[1][i])
                    if convenio==True:
                        total_compra=total_compra-(total_compra*0.10)
                        despachos["total_compra"].append(total_compra)
                    else:
                        despachos["total_compra"].append(total_compra)

            input("Presione una tecla para continuar...")
            
            
        if opcion=="c":
            os.system ("cls")
            tamaño = 4
            print("Lista de Huevos:")
            for i in range(tamaño):                
                print("Huevo de ", huevos[0][i]," - Valor: $",huevos[1][i])
        
            input("Presione una tecla para continuar...")
            

        if opcion=="d":
            os.system ("cls")
            print("Despachos:")
            opcion_listar=""
            while opcion_listar!="3":
                print("1) Buscar por ID")
                print("2) Listar todos los despachos")
                print("3) Salir")
                opcion_listar=input("Ingrese Opcion: ")
                if opcion_listar=="1":
                    os.system ("cls")
                    opcion_listado=int(input("Ingrese ID a buscar: "))
                    for i in range(len(despachos["ID"])):
                        id_de_despachos=despachos["ID"][i]
                        if opcion_listado==id_de_despachos:
                            print("---------------------------------------------------")
                            print("ID: ",despachos["ID"][i])
                            print("Rut: ",despachos["Rut"][i])
                            print("Nombre: ",despachos["Nombre"][i])
                            print("Huevo seleccionado: ",despachos["Eleccion_huevo"][i])
                            print("¿Convenio?: ",despachos["convenio"][i])
                            print("Dirección: ",despachos["direccion"][i])
                            print("Fecha de entrega: ",despachos["fecha_despacho"][i])
                            print("Huevos comprados: ",despachos["cantidad_huevos"][i])
                            print("Total de la compra: $",despachos["total_compra"][i])
                            print("---------------------------------------------------")
                            input("Presione una tecla para continuar...")
                if opcion_listar=="2":
                    os.system ("cls")
                    for i in range(len(despachos["ID"])):
                        print("---------------------------------------------------")
                        print("ID: ",despachos["ID"][i])
                        print("Rut: ",despachos["Rut"][i])
                        print("Nombre: ",despachos["Nombre"][i])
                        print("Huevo seleccionado: ",despachos["Eleccion_huevo"][i])
                        print("¿Convenio?: ",despachos["convenio"][i])
                        print("Dirección: ",despachos["direccion"][i])
                        print("Fecha de entrega: ",despachos["fecha_despacho"][i])
                        print("Huevos comprados: ",despachos["cantidad_huevos"][i])
                        print("Total de la compra: $",despachos["total_compra"][i])
                        print("---------------------------------------------------")
                    input("Presione una tecla para continuar...")
                
            
            
    
